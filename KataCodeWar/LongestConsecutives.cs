﻿using System.Linq;

namespace KataCodeWar
{
    public class LongestConsecutives
    {
        public static int Persistence(long n)
        {
            int count = 0;
            while (n > 9)
            {
                count++;
                n = n.ToString().Select(digit => int.Parse(digit.ToString())).Aggregate((x, y) => x * y);
            }
            return count;
        }
    }
}