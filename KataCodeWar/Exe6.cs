﻿namespace KataCodeWar
{
    public class StringMerger
    {
        public static bool isMerge(string s, string part1, string part2)
        {
            if (s.Length == 0)
            {
                return false;
            }

            var index1 = 0;
            var index2 = 0;
            foreach (var item in s)
            {
                var flag = false;

                if (index1 < part1.Length && part1.Length > 0)
                {
                    if (item.Equals(part1[index1]))
                    {
                        index1++;
                        continue;
                    }
                }

                if (index2 < part2.Length && part2.Length > 0)
                {
                    if (item.Equals(part2[index2]))
                    {
                        index2++;
                        continue;
                    }

                    return false;
                }
            }

            return true;
        }
    }
}