﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace KataCodeWar
{
    public class Exe4
    {
        public static long QueueTime(int[] customers, int n)
        {
            var registers = new List<int>(Enumerable.Repeat(0, n));

            foreach (int cust in customers)
            {
                registers[registers.IndexOf(registers.Min())] += cust;
            }
            return registers.Max();
        }
    }
}