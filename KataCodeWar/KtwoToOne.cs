﻿using System.Linq;

namespace KataCodeWar
{
    public class KtwoToOne
    {
        public static string Longest(string s1, string s2)
        {
            return string.Concat(string.Concat(s1, s2).OrderBy(c => c).Distinct());
        }
    }
}