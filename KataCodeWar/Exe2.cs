﻿using System.Linq;

namespace KataCodeWar
{
    public class Exe2
    {
        public static int GetLengthOfMissingArray(object[][] arrayOfArrays)
        {
            if (arrayOfArrays == null)
            {
                return 0;
            }

            if (arrayOfArrays.Length == 0)
            {
                return 0;
            }

            foreach (var item in arrayOfArrays)
            {
                if (item == null)
                {
                    return 0;
                }
            }
            var arr = arrayOfArrays.OrderBy(a => a.Length).ToArray();

            for (var i = 0; i < arr.Length; i++)
            {
                if (arr[i].Length == 0)
                {
                    return 0;
                }

                if (arr[i].Length + 1 != arr[i + 1].Length)
                {
                    return arr[i].Length + 1;
                }
            }

            return 0;
        }
    }
}