﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KataCodeWar
{
   public class Multiple3and5
    {
        public static int Solution(int value)
        {
            int sum = 0;

            if (value < 2)
            {
                return 0;
            }

            for (int i = 3; i < value; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sum += i;
                }
            }
                    
            return sum;
        }
    }
}
