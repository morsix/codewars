﻿using System.Linq;

namespace KataCodeWar
{
    public class Exe5
    {
        public static string Decrypt(string encryptedText, int n)
        {
            return encryptedText;
        }

        public static string Encrypt(string text, int n)
        {
            if (n == 0 || n > text.Length)
            {
                return text;
            }
            var s = text;
            var str1 = string.Empty;
            var str2 = string.Empty;
            while (s != string.Empty)
            {
                str1 += s.Substring(n, 1);
                str2 += s.Substring(0, n - 1);
                if (s.Length <= n)
                {
                    break;
                }
                s = s.Substring(n);
            }

            return str1 + str2;
        }
    }
}