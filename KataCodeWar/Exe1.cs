﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KataCodeWar
{
    public class Exe1
    {
        public static int FindEvenIndex(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                int sum1 = 0;
                int sum2 = 0;
                for (int j = 0; j < i; j++)
                {
                    sum1 += arr[j];
                }
                for (int k = arr.Length - 1; k > i; k--)
                {
                    sum2 += arr[k];
                }
                if (sum1 == sum2)
                {
                    return i;
                }

            }
            return -1;
        }
    }
}
