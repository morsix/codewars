﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KataCodeWar
{
    public class Converter
    {
        public string Convert(string input, string source, string target)
        {  
            long dec = sourceToDecimal(input, source);
            string result = decimalToTarget(dec, target);

            return result;
        }

        private long sourceToDecimal(string input, string source)
        {
            int alphabetBase = source.Length;
            long decimalNumber = 0;
            int power = 0;

            for (int i = input.Length - 1; i >= 0; i--)
            {
                decimalNumber += (long)Math.Pow(alphabetBase, power) * source.IndexOf(input[i]);
                power++;
            }

            return decimalNumber;
        }

        private string decimalToTarget(long decimalValue, string target)
        {
            int alphabetBase = target.Length;
            StringBuilder result = new StringBuilder("");
            char[] chars = target.ToCharArray();

            if (decimalValue == 0)
            {
                return target.Substring(0, 1);
            }

            while (decimalValue != 0)
            {
                long x = decimalValue % alphabetBase;
                result = result.Append(chars[x]);
                decimalValue = decimalValue / alphabetBase;
            }

            char[] s = result.ToString().ToCharArray();
            Array.Reverse(s);

            return new string(s);
        }
    }

    public class Alphabet
    {
        public const string BINARY = "01";
        public const string OCTAL = "01234567";
        public const string DECIMAL = "0123456789";
        public const string HEXA_DECIMAL = "0123456789abcdef";
        public const string ALPHA_LOWER = "abcdefghijklmnopqrstuvwxyz";
        public const string ALPHA_UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const string ALPHA = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const string ALPHA_NUMERIC = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
}

// best practice

//using System;
//using System.Linq;
//public class Kata
//{
//    public string Convert(string i, string s, string t)
//    {
//        var a = i.Select((x, n) => s.IndexOf(x) * (long)Math.Pow(s.Length, i.Length - 1 - n)).Sum();
//        string rs;
//        for (rs = ""; a > 0; a /= t.Length) rs = t[(int)(a % t.Length)] + rs;
//        return i == "0" ? t[0] + "" : rs;
//    }
//}
