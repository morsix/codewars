﻿using System.Text.RegularExpressions;

namespace KataCodeWar
{
    public class Exe3
    {
        // return str.Split(' ').Select((s, i) => s.Substring(1) + s.Substring(0, 1) + "ay").ToString();
        public static string PigIt(string str)
        {

           return Regex.Replace(str, @"(\w)(\w+)", @"$2$1ay");
        }
    }
}