﻿using System;
using KataCodeWar;
using NUnit.Framework;

namespace KataCodeWarTest
{
    [TestFixture]
    public static class LogestConsecutivesTest
    {
        [Test]
        public static void Test1()
        {
            Console.WriteLine("****** Basic Tests");
            Assert.AreEqual(3, LongestConsecutives.Persistence(39));
            Assert.AreEqual(0, LongestConsecutives.Persistence(4));
            Assert.AreEqual(2, LongestConsecutives.Persistence(25));
            Assert.AreEqual(4, LongestConsecutives.Persistence(999));
        }
    }
}