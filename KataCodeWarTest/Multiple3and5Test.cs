﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KataCodeWar;
using NUnit.Framework;

namespace KataCodeWarTest
{
    [TestFixture]
    public class Multiple3and5Test
    {
        [Test]
        public void Test()
        {
            Assert.AreEqual(23, Multiple3and5.Solution(10));
        }
    }
}
