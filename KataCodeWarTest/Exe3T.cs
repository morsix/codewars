﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KataCodeWar;

using NUnit.Framework;

namespace KataCodeWarTest
{
   public class Exe3T
    {
        [Test]
        public void KataTests()
        {
            Assert.AreEqual("igPay atinlay siay oolcay", Exe3.PigIt("Pig latin is cool"));
            Assert.AreEqual("hisTay siay ymay tringsay", Exe3.PigIt("This is my string"));
        }
    }
}
