﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KataCodeWar;

using NUnit.Framework;

namespace KataCodeWarTest
{
   public class Exe5T
    {
        [Test]
        public void EncryptExampleTests()
        {
            Assert.AreEqual("This is a test!", Exe5.Encrypt("This is a test!", 0));
            Assert.AreEqual("hsi  etTi sats!", Exe5.Encrypt("This is a test!", 1));
            Assert.AreEqual("s eT ashi tist!", Exe5.Encrypt("This is a test!", 2));
            Assert.AreEqual(" Tah itse sits!", Exe5.Encrypt("This is a test!", 3));
            Assert.AreEqual("This is a test!", Exe5.Encrypt("This is a test!", 4));
            Assert.AreEqual("This is a test!", Exe5.Encrypt("This is a test!", -1));
            Assert.AreEqual("hskt svr neetn!Ti aai eyitrsig", Exe5.Encrypt("This kata is very interesting!", 1));
        }

        [Test]
        public void DecryptExampleTests()
        {
            Assert.AreEqual("This is a test!", Exe5.Decrypt("This is a test!", 0));
            Assert.AreEqual("This is a test!", Exe5.Decrypt("hsi  etTi sats!", 1));
            Assert.AreEqual("This is a test!", Exe5.Decrypt("s eT ashi tist!", 2));
            Assert.AreEqual("This is a test!", Exe5.Decrypt(" Tah itse sits!", 3));
            Assert.AreEqual("This is a test!", Exe5.Decrypt("This is a test!", 4));
            Assert.AreEqual("This is a test!", Exe5.Decrypt("This is a test!", -1));
            Assert.AreEqual("This kata is very interesting!", Exe5.Decrypt("hskt svr neetn!Ti aai eyitrsig", 1));
        }

        [Test]
        public void EmptyTests()
        {
            Assert.AreEqual("", Exe5.Encrypt("", 0));
            Assert.AreEqual("", Exe5.Decrypt("", 0));
        }

        [Test]
        public void NullTests()
        {
            Assert.AreEqual(null, Exe5.Encrypt(null, 0));
            Assert.AreEqual(null, Exe5.Decrypt(null, 0));
        }
    }
}
