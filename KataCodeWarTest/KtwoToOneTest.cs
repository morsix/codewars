﻿using KataCodeWar;
using NUnit.Framework;

namespace KataCodeWarTest
{
    [TestFixture]
    public class KtwoToOneTest
    {
        [Test]
        public static void test1()
        {
            Assert.AreEqual("aehrsty", KtwoToOne.Longest("aretheyhere", "yestheyarehere"));
            Assert.AreEqual("abcdefghilnoprstu", KtwoToOne.Longest("loopingisfunbutdangerous", "lessdangerousthancoding"));
            Assert.AreEqual("acefghilmnoprstuy", KtwoToOne.Longest("inmanylanguages", "theresapairoffunctions"));
        }
    }
}
